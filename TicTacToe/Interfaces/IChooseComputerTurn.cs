namespace TicTacToe.Interfaces
{
    public interface IChooseComputerTurn
    {
        bool ChangeBoard(string moveFor);
        bool ComputerTurn();
    }
}