namespace TicTacToe.Interfaces
{
    public interface ICheckingUserAnswer
    {
        void CheckNumberOfLettersInWord(char[] array, out string letter, out int number);
        string SelectTheNumberOfTheBoardArea(string player);
    }
}