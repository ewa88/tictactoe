namespace TicTacToe.Interfaces
{
    public interface IProgramLoop
    {
        void Execute();
    }
}