﻿namespace TicTacToe.Interfaces
{
    public interface IComputerLogic
    {
        bool CheckHorizontalSequence(int n, out int rowElement, out int columnElement);
        bool CheckVerticalSequence(int n, out int rowElement, out int columnElement);
        bool CheckLeftSlantSequence(int n, out int rowElement, out int columnElement);
        bool CheckRightSlantSequence(int n, out int rowElement, out int columnElement);
        bool CheckAvailabilityOfFields(int n, out int rowElement, out int columnElement);
    }
}