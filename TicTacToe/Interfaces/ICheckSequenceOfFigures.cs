namespace TicTacToe.Interfaces
{
    public interface ICheckSequenceOfFigures
    {
        bool CheckTheWinner(string figure);
        bool CheckIfSomeoneWonTheGame(int n, string figure);
        bool CheckHorizontalPositionOfFigures(int n, string figure);
        bool CheckVerticalPositionOfFigures(int n, string figure);
        bool CheckLeftSlantPositionOfFigures(int n, string figure);
        bool CheckRightSlantPositionOfFigures(int n, string figure);
        bool CheckIfIsDraw();
    }
}