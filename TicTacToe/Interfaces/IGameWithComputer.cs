﻿namespace TicTacToe.Interfaces
{
    public interface IGameWithComputer
    {
        void UserPlayWithComputer();
        void GameMoveComputer(/*ChooseComputerTurn computerTurn, */string figure);
    }
}