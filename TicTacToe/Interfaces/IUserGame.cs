﻿namespace TicTacToe.Interfaces
{
    public interface IUserGame
    {
        void GameForTwoUsers();
        void UserGameMove(string moveFor);
    }
}