namespace TicTacToe.Interfaces
{
    public interface IChangesOnGameBoard
    {
        void PrepareNumbersToChangeBoard(string player);
        bool ChangeBoard(string moveFor);
    }
}