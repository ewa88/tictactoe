namespace TicTacToe.Interfaces
{
    public interface IPrintGameBoard
    {
        void PrintDefaultBoard(string message);
        void CreateGameBoard();
    }
}