﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class ComputerLogic : IComputerLogic
    {
        // algorytm sprawdza czy przeciwnik wpisał kombinacje dwóch X (w wierszu, kolumnie, po obu skosach)
        // następnie sprawdza czy pole z którejkolwiek strony jest puste, jeśli tak to umieszcza tam kółko
        // tak aby przeszkodzić w utworzeniu wygranej sekwencji,
        // jeśli przeciwnik nie wpisał żadnej takiej kombinacji to komputer wypełnia pola tablicy
        // po kolei od tyłu (tak dla zmyłki :)
        public bool CheckHorizontalSequence(int n, out int rowElement, out int columnElement)
        {
            bool sequenceExist = false;
            rowElement = 0;
            columnElement = 0;

            for (int i = 0; i <= BoardSizeSettings.BoardSize - 1; i++)
            {
                for (int j = 0; j <= BoardSizeSettings.BoardSize - 2; j++)
                {
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" &&
                        ChangesOnGameBoard.TicTacArrays[i, j + 1] == "X")
                    {
                        if (j <= (BoardSizeSettings.BoardSize - 3))//zeby j+2 nie przekraczało max rozmiaru tablicy
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i, j + 2] == " ")
                            {
                                rowElement = i;
                                columnElement = j + 2;
                                sequenceExist = true;
                            }
                        }
                        if (j >= 1)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i, j - 1] == " ")
                            {
                                rowElement = i;
                                columnElement = j - 1;
                                sequenceExist = true;
                            }
                        }
                    }
                }
            }
            return sequenceExist;
        }

        public bool CheckVerticalSequence(int n, out int rowElement, out int columnElement)
        {
            bool sequenceExist = false;
            rowElement = 0;
            columnElement = 0;

            for (int j = 0; j <= BoardSizeSettings.BoardSize - 1; j++)
            {
                for (int i = 0; i <= BoardSizeSettings.BoardSize - 2; i++)
                {

                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" &&
                        ChangesOnGameBoard.TicTacArrays[i + 1, j] == "X")
                    {
                        if (i <= BoardSizeSettings.BoardSize - 3)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i + 2, j] == " ")
                            {
                                rowElement = i + 2;
                                columnElement = j;
                                sequenceExist = true;
                            }
                        }
                        if (i >= 1)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i - 1, j] == " ")
                            {
                                rowElement = i - 1;
                                columnElement = j;
                                sequenceExist = true;
                            }
                        }
                    }
                }
            }
            return sequenceExist;
        }

        public bool CheckLeftSlantSequence(int n, out int rowElement, out int columnElement)
        {
            bool sequenceExist = false;
            rowElement = 0;
            columnElement = 0;
            for (int i = 0; i <= BoardSizeSettings.BoardSize - 2; i++)
            {
                for (int j = 0; j <= BoardSizeSettings.BoardSize - 2; j++)
                {
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" &&
                        ChangesOnGameBoard.TicTacArrays[i + 1, j + 1] == "X")
                    {
                        if (i + 2 <= BoardSizeSettings.BoardSize - 1 && j + 2 <= BoardSizeSettings.BoardSize - 1)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i + 2, j + 2] == " ")
                            {
                                rowElement = i + 2;
                                columnElement = j + 2;
                                sequenceExist = true;
                            }
                        }
                        if (i >= 1 && j >= 1)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i - 1, j - 1] == " ")
                            {
                                rowElement = i - 1;
                                columnElement = j - 1;
                                sequenceExist = true;
                            }
                        }

                    }
                }
            }
            return sequenceExist;
        }

        public bool CheckRightSlantSequence(int n, out int rowElement, out int columnElement)
        {
            bool sequenceExist = false;
            rowElement = 0;
            columnElement = 0;
            for (int i = 0; i <= BoardSizeSettings.BoardSize - 2; i++)
            {

                for (int j = 1; j <= BoardSizeSettings.BoardSize - 1; j++)//od 1 bo jak odejmie 1 bedzie 0
                {
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" &&
                        ChangesOnGameBoard.TicTacArrays[i + 1, j - 1] == "X")
                    {
                        if (i + 2 <= BoardSizeSettings.BoardSize - 1 && j >= 2)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i + 2, j - 2] == " ")
                            {
                                rowElement = i + 2;
                                columnElement = j - 2;
                                sequenceExist = true;
                            }
                        }
                        if (i >= 1 && j + 1 <= BoardSizeSettings.BoardSize - 1)
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i - 1, j + 1] == " ")
                            {
                                rowElement = i - 1;
                                columnElement = j + 1;
                                sequenceExist = true;
                            }
                        }
                    }

                }
            }
            return sequenceExist;
        }

        public bool CheckAvailabilityOfFields(int n, out int rowElement, out int columnElement)
        {
            bool sequenceExist = false;
            rowElement = 0;
            columnElement = 0;

            for (int i = 0; i <= BoardSizeSettings.BoardSize - 1; i++)
            {
                for (int j = 0; j <= BoardSizeSettings.BoardSize - 1; j++)
                {
                    if (ChangesOnGameBoard.TicTacArrays[i, j] != "X" && ChangesOnGameBoard.TicTacArrays[i, j] != "O")
                    {
                        rowElement = i;
                        columnElement = j;
                        sequenceExist = true;
                    }
                }
            }
            return sequenceExist;
        }
    }
}



