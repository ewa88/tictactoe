﻿using System;
using TicTacToe.IoHelper;

namespace TicTacToe
{
    public class BoardSizeSettings
    {
        public static int BoardSize;
        public static int NumberOfWinningFigures;

        public static void CheckIfUserWantToChangeSettings()
        {
            bool loop = true;
            var answer = ConsoleReadHelper.GetAnswer("Do you want to change game settings (yes/no)");
            while (loop)
            {
                if (!String.IsNullOrEmpty(answer))
                {
                    if (answer == "no" || answer == "yes")
                    {
                        if (answer == "yes")
                        {
                            BoardSize = ConsoleReadHelper.GetSettingsAnswer("Board size");
                            NumberOfWinningFigures = ConsoleReadHelper.GetSettingsAnswer("Number of winning figures: ");
                            loop = false;
                        }
                        else
                        {
                            loop = false;
                        }
                    }
                    else
                    {
                        answer = ConsoleReadHelper.GetAnswer("Error, write correct answer (yes/no).");
                    }
                }
                else
                {
                    answer = ConsoleReadHelper.GetAnswer("Error, write correct answer (yes/no).");
                }
            }
        }

        public static void DefaultSettings()
        {
            BoardSize = 3;
            NumberOfWinningFigures = 3;
        }
    }
}
