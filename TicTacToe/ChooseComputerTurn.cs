﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class ChooseComputerTurn : IChooseComputerTurn
    {
        public int RowElement;
        public int ColumnElement;

        private IComputerLogic _computerLogic;

        public ChooseComputerTurn(IComputerLogic computerLogic)
        {
            _computerLogic = computerLogic;
        }
        public bool ChangeBoard(string figure)
        {
            bool usedField=false;
            if(ComputerTurn())
            {
                ChangesOnGameBoard.TicTacArrays[RowElement, ColumnElement] = figure;
                usedField = true;
            }

            return usedField;
        }
        public bool ComputerTurn()
        {
            bool numbersAreChoosed = false;
            if (_computerLogic.CheckHorizontalSequence(BoardSizeSettings.BoardSize,
                out int rowNumberFromHorizontalMethod, out int columnNumberFromHorizontalMethod))
            {
                RowElement = rowNumberFromHorizontalMethod;
                ColumnElement = columnNumberFromHorizontalMethod;
                numbersAreChoosed = true;
            }
            else
            {
                if (_computerLogic.CheckVerticalSequence(BoardSizeSettings.BoardSize,
                    out int rowNumberFromVerticalMethod,
                    out int columnNumberFromVerticalMethod))
                {
                    RowElement = rowNumberFromVerticalMethod;
                    ColumnElement = columnNumberFromVerticalMethod;
                    numbersAreChoosed = true;
                }
                else
                {
                    if (_computerLogic.CheckLeftSlantSequence(BoardSizeSettings.BoardSize,
                        out int rowNumberFromLeftSlantMethod,
                        out int columnNumberFromLeftSlantMethod))
                    {
                        RowElement = rowNumberFromLeftSlantMethod;
                        ColumnElement = columnNumberFromLeftSlantMethod;
                        numbersAreChoosed = true;
                    }
                    else
                    {
                        if (_computerLogic.CheckRightSlantSequence(BoardSizeSettings.BoardSize,
                            out int rowNumberFromRightSlantMethod,
                            out int columnNumberFromRightSlantMethod))
                        {
                            RowElement = rowNumberFromRightSlantMethod;
                            ColumnElement = columnNumberFromRightSlantMethod;
                            numbersAreChoosed = true;
                        }
                        else
                        {

                            if (_computerLogic.CheckAvailabilityOfFields(BoardSizeSettings.BoardSize,
                                out int rowNumberFromChooseMethod,
                                out int columnNumberFromChooseMethod))
                            {
                                RowElement = rowNumberFromChooseMethod;
                                ColumnElement = columnNumberFromChooseMethod;
                                numbersAreChoosed = true;
                            }
                        }
                    }
                }
            }
            return numbersAreChoosed;
        }
    }
}
