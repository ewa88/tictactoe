﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class UserGame : IUserGame
    {
        private IPrintGameBoard _printGameBoard;
        private IChangesOnGameBoard _changesOnGameBoard;
        private ICheckSequenceOfFigures _checkSequenceOfFigures;

        public UserGame(IPrintGameBoard printGameBoard, IChangesOnGameBoard changesOnGameBoard, 
            ICheckSequenceOfFigures checkSequenceOfFigures)
        {
            _printGameBoard = printGameBoard;
            _changesOnGameBoard = changesOnGameBoard;
            _checkSequenceOfFigures = checkSequenceOfFigures;
        }
        public void GameForTwoUsers()
        {
            Array.Clear(ChangesOnGameBoard.TicTacArrays, 0, ChangesOnGameBoard.TicTacArrays.Length);
            _printGameBoard.PrintDefaultBoard("Default gameboard: ");
            BoardSizeSettings.CheckIfUserWantToChangeSettings();
            Console.Clear();
            _printGameBoard.PrintDefaultBoard("Gameboard: ");
            Console.WriteLine("---------------- TIC TAC TOE ----------------");

            bool loop = false;
            while (loop == false)
            {
                _changesOnGameBoard.PrepareNumbersToChangeBoard("X"); ;
                UserGameMove("X");
                var xBool = _checkSequenceOfFigures.CheckTheWinner("X");
                loop = xBool;
                if (xBool == false)
                {
                    _changesOnGameBoard.PrepareNumbersToChangeBoard("O");
                    UserGameMove("O");
                    var oBool = _checkSequenceOfFigures.CheckTheWinner("O");
                    loop |= oBool;
                }
            }
        }

        public void UserGameMove(string moveFor)
        {
            if (_changesOnGameBoard.ChangeBoard(moveFor))
            {
                Console.Clear();
                _printGameBoard.CreateGameBoard();
            }
            Console.WriteLine("");
        }
    }
}

