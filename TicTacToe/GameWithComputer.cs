﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
  public class GameWithComputer : IGameWithComputer
  {
      private IPrintGameBoard _printGameBoard;
      private ICheckSequenceOfFigures _checkSequenceOfFigures;
      private IChooseComputerTurn _chooseComputerTurn;
      private IChangesOnGameBoard _changesOnGameBoard;
      private IUserGame _userGame;

      public GameWithComputer(IPrintGameBoard printGameBoard, ICheckSequenceOfFigures checkSequenceOfFigures,
          IChooseComputerTurn chooseComputerTurn, IChangesOnGameBoard changesOnGameBoard,IUserGame userGame)
      {
          _printGameBoard = printGameBoard;
          _checkSequenceOfFigures = checkSequenceOfFigures;
          _chooseComputerTurn = chooseComputerTurn;
          _changesOnGameBoard = changesOnGameBoard;
          _userGame = userGame;
      }
        public void UserPlayWithComputer()
        {
            Array.Clear(ChangesOnGameBoard.TicTacArrays,0,ChangesOnGameBoard.TicTacArrays.Length);
            _printGameBoard.PrintDefaultBoard("Default gameboard: ");

            BoardSizeSettings.CheckIfUserWantToChangeSettings();
            Console.Clear();
            _printGameBoard.PrintDefaultBoard("Gameboard: ");
            Console.WriteLine("---------------- TIC TAC TOE ----------------");

            bool loop = false;
            while (loop == false)
            {
                _changesOnGameBoard.PrepareNumbersToChangeBoard("X"); ;
                _userGame.UserGameMove("X");
                var xBool = _checkSequenceOfFigures.CheckTheWinner("X");
                loop = xBool;
               
                if (xBool == false)
                {
                    GameMoveComputer("O");
                    var oBool = _checkSequenceOfFigures.CheckTheWinner("O");
                    loop |= oBool;
                }
            }
        }
        public void GameMoveComputer(string figure)
        {
            if (_chooseComputerTurn.ChangeBoard(figure))
            {
                Console.Clear();
               _printGameBoard.CreateGameBoard();
            }
            Console.WriteLine("");
        }
    }
}

