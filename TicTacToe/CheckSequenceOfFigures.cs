﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{ 
    public class CheckSequenceOfFigures : ICheckSequenceOfFigures
    {
        public bool CheckTheWinner(string figure)
        {
         return CheckIfSomeoneWonTheGame(BoardSizeSettings.NumberOfWinningFigures, figure);
        }

        public bool CheckIfSomeoneWonTheGame(int n, string figure)
        {
            var returnBool = CheckHorizontalPositionOfFigures(n, figure);
            returnBool |= CheckVerticalPositionOfFigures(n, figure);
            returnBool |= CheckRightSlantPositionOfFigures(n, figure);
            returnBool |= CheckLeftSlantPositionOfFigures(n, figure);
            returnBool |= CheckIfIsDraw();
            return returnBool;
        }
        public bool CheckHorizontalPositionOfFigures(int n, string figure)
        {
            bool isWin = false;

            for (int i = 0; i <= BoardSizeSettings.BoardSize - 1; i++)
            {
                var count = 1;
                for (int j = 0; j <= BoardSizeSettings.BoardSize - 2; j++)
                {               
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == figure &&
                        ChangesOnGameBoard.TicTacArrays[i, j + 1] == figure)
                    {
                        count++;
                        if (count == n)
                        {
                            Console.WriteLine(figure + " won.");
                            isWin = true;
                            Console.ReadKey();
                        }

                    }
                }
            }
            return isWin;
        }
        public bool CheckVerticalPositionOfFigures(int n, string figure)
        {
            bool isWin = false;
            
            for (int j = 0; j <= BoardSizeSettings.BoardSize - 1; j++)
            {
                var count = 1;
                for (int i = 0; i <= BoardSizeSettings.BoardSize - 2; i++)
                {                 
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == figure &&
                        ChangesOnGameBoard.TicTacArrays[i + 1, j] == figure)
                    {
                        count++;
                        if (count == n)
                        {
                            Console.WriteLine(figure + " won.");                          
                            isWin = true;
                            Console.ReadKey();
                        }
                    }
                    else
                    {
                        count = 1;
                    }
                }
            }
            return isWin;
        }

        public bool CheckLeftSlantPositionOfFigures(int n, string figure)
        {
            bool isWin = false;
      
            for (int i = 0; i <= BoardSizeSettings.BoardSize - 2; i++)
            {
                for (int j = 0; j <= BoardSizeSettings.BoardSize - 2; j++)
                {
                    var count = 1;
                    bool loop = true;
                    while (loop)
                    {                    
                        if ((i + count<=BoardSizeSettings.BoardSize - 1  ) && (BoardSizeSettings.BoardSize - 1 >= j + count))
                        {

                            if (ChangesOnGameBoard.TicTacArrays[i + count - 1, j + count - 1] == figure &&
                                ChangesOnGameBoard.TicTacArrays[i + count, j + count] == figure)
                            {
                                count++;
                                if (count == n)
                                {
                                    Console.WriteLine(figure + " won.");
                                    isWin = true;
                                    Console.ReadKey();
                                    loop = false;
                                }
                            }
                            else
                            {
                                loop = false;
                            }
                        }
                        else
                        {
                            loop = false;
                        }
                    }
                }
            }
            return isWin;
        }

        public bool CheckRightSlantPositionOfFigures(int n, string figure)
        {
            bool isWin = false;
            for (int i = 0; i <= BoardSizeSettings.BoardSize - 1; i++)
            {

                for (int j = 0; j <= BoardSizeSettings.BoardSize - 1; j++)
                {
                    var count = 1;
                    var loop = true;
                    while (loop)
                    {
                        if ((i + count<=BoardSizeSettings.BoardSize - 1) &&
                            (j - count>=0))
                        {
                            if (ChangesOnGameBoard.TicTacArrays[i + count - 1, j - (count - 1)] == figure &&
                                ChangesOnGameBoard.TicTacArrays[i + count, j - count] == figure)
                            {
                                count++;
                                if (count == n)
                                {
                                    Console.WriteLine(figure + " won.");
                                    isWin = true;
                                    loop = false;
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                loop = false;
                            }
                        }
                        else
                        {
                            loop = false;
                        }
                    }
                }            
            }
            return isWin;
        }
        public bool CheckIfIsDraw()
        {
            bool isDraw = false;
            var count = 1;
            int lenght = BoardSizeSettings.BoardSize ;

            for (int i = 0; i <lenght ; i++)
            {

                for (int j = 0; j < lenght; j++)
                {
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" || ChangesOnGameBoard.TicTacArrays[i, j] == "O")
                    {

                        if (count == lenght*lenght)
                        {
                            Console.WriteLine("The game ended in a draw.");
                            isDraw = true;
                            Console.ReadKey();
                        }
                        count++;
                    }
                }
            }
            return isDraw;
        }
    }
}
