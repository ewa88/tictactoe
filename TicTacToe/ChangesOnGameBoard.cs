﻿using System;
using System.Collections.Generic;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class ChangesOnGameBoard : IChangesOnGameBoard
    {
        public static int RowNumber;
        public static int ColumnNumber;

        private ICheckingUserAnswer _checkingUserAnswer;

        public ChangesOnGameBoard(ICheckingUserAnswer checkingUserAnswer)
        {
            _checkingUserAnswer = checkingUserAnswer;
        }

        public static string[,] TicTacArrays = new string[10, 10];
        
        public static Dictionary<string, int> LetterDictionary = new Dictionary<string, int>()
        {
            {"A", 0},
            {"B", 1},
            {"C", 2},
            {"D", 3},
            {"E", 4},
            {"F", 5},
            {"G", 6},
            {"H", 7},
            {"I", 8},
            {"J", 9}
        };

    public void PrepareNumbersToChangeBoard(string player)
        {
            var answer = _checkingUserAnswer.SelectTheNumberOfTheBoardArea(player);
            var array = answer.ToCharArray();
            string letter;
            int rowNumber;
            _checkingUserAnswer.CheckNumberOfLettersInWord(array, out letter, out rowNumber);
            var columnNumber = LetterDictionary[letter];

            RowNumber = rowNumber;
            ColumnNumber = columnNumber;
        }

        public bool ChangeBoard(string moveFor)
        {
            bool usedField = false;
            string figure = moveFor;
           bool loop = true;
           while (loop)
           {
               if (TicTacArrays[RowNumber, ColumnNumber] == "X" || TicTacArrays[RowNumber, ColumnNumber] == "O")
               {
                   Console.Write("This field is used.\n");
                   PrepareNumbersToChangeBoard(moveFor);
               }
               else
               {
                   TicTacArrays[RowNumber, ColumnNumber] = figure;
                   loop = false;
                   usedField = true;
               }
           }
           return usedField;
       }    
    }
}
