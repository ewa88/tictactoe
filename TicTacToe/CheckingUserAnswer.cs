﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class CheckingUserAnswer : ICheckingUserAnswer
    {
        public void CheckNumberOfLettersInWord(char[] array, out string letter, out int number)
        {
            if (array.Length > 2)
            {
                var part1 = array[1].ToString();
                var part2 = array[2].ToString();
                number = Int32.Parse(part1 + part2);
                letter = array[0].ToString().ToUpper();
            }
            else
            {
                letter = array[0].ToString().ToUpper();
                number = Int32.Parse(array[1].ToString());
            }
        }

        public string SelectTheNumberOfTheBoardArea(string player)
        {
            string answer = "";
            bool answerLoop = true;
            while (answerLoop)
            {
                Console.WriteLine("Choose number of the board (player {0}): ", player);
                answer = Console.ReadLine();
                if (!String.IsNullOrEmpty(answer))
                {
                    Regex firstRegex = new Regex(@"^[A-j]{1}[0-9]{1}$");
                    Regex secondRegex = new Regex(@"^[A-j]{1}[0-9]{1}[0]$");
                    if (firstRegex.IsMatch(answer) || secondRegex.IsMatch(answer))
                    {
                        var array = answer.ToCharArray();

                        string letter;
                        int number;
                        CheckNumberOfLettersInWord(array, out letter, out number);
                        var nextNumber = ChangesOnGameBoard.LetterDictionary[letter];
                        if (nextNumber < BoardSizeSettings.BoardSize &&
                            number < BoardSizeSettings.BoardSize)
                        {
                            answerLoop = false;
                        }
                        else
                        {
                            Console.WriteLine("There is on such field. ");
                        }
                    }
                }

            }
            return answer;
        }

    }
}
