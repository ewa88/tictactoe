﻿using System;
using Ninject;
using TicTacToe.Interfaces;
using TicTacToe.NinjectModule;

namespace TicTacToe
{
    public class Program
    {
        static void Main(string[] args)
        {
            Ninject.IKernel kernal = new StandardKernel(new ProgramModule());
            kernal.Get<IProgramLoop>().Execute();
            
        }
    }
}
