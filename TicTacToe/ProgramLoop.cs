﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
   public class ProgramLoop : IProgramLoop
   {
       private IGameWithComputer _gameWithComputer;
       private IUserGame _userGame;
       public ProgramLoop(IGameWithComputer gameWithComputer, IUserGame userGame)
       {
           _gameWithComputer = gameWithComputer;
           _userGame = userGame;
       }
        public void Execute()
        {      
          BoardSizeSettings.DefaultSettings();
            bool loop = true;
            while (loop)
            {              
                Console.WriteLine("Choose command (play game, game for two, exit) :");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "play game":
                        _gameWithComputer.UserPlayWithComputer();
                        break;
                    case "game for two":
                        _userGame.GameForTwoUsers();
                        break;
                    case "exit":
                        loop = false;
                        break;
                    default:
                        Console.WriteLine("No such command.");
                        break;
                }
            }
        }
    }
}
