﻿using System;

namespace TicTacToe.IoHelper
{
    public class ConsoleReadHelper
    {
        public static string GetAnswer(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        public static int GetInt(string message)
        {
            int number = 0;
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Error, try again. ");
            }
            return number;
        }
        public static int GetSettingsAnswer(string message)
        {
            int number = 0;
            bool loop = true;
            while (loop)
            {
                number = GetInt(message);
                if (!(number <= 2 || number >= 11))
                {
                    loop = false;
                }
            }
            return number;
        }
    }
}
