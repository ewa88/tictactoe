﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Interfaces;

namespace TicTacToe.NinjectModule
{
    public class ProgramModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IComputerLogic>().To<ComputerLogic>();
            Bind<IPrintGameBoard>().To<PrintGameBoard>();
            Bind<IChangesOnGameBoard>().To<ChangesOnGameBoard>();
            Bind<ICheckSequenceOfFigures>().To<CheckSequenceOfFigures>();
            Bind<IChooseComputerTurn>().To<ChooseComputerTurn>();
            Bind<IGameWithComputer>().To<GameWithComputer>();
            Bind<IUserGame>().To<UserGame>();
            Bind<IProgramLoop>().To<ProgramLoop>();
            Bind<ICheckingUserAnswer>().To<CheckingUserAnswer>();
        }
    }
}
