﻿using System;
using TicTacToe.Interfaces;

namespace TicTacToe
{
    public class PrintGameBoard : IPrintGameBoard
    {
        public void PrintDefaultBoard(string message)
        {
            Console.WriteLine("\n" +message+ "\n");
            CreateGameBoard();
            Console.WriteLine("\n");
        }
        public void CreateGameBoard()
        {
            int x = BoardSizeSettings.BoardSize;

            char letter = 'A';
            Console.Write(" ");
            for (int i = 0; i < x; i++)
            {
                Console.Write(" " + (letter++) + "  ");
            }
            Console.WriteLine();
            //wiersz
            for (int i = 0; i < x; i++)
            {
                Console.Write(i);
                //kolumna
                for (int j = 0; j < x; j++)
                {
                    Console.Write(" ");
                    if (ChangesOnGameBoard.TicTacArrays[i, j] == "X" || ChangesOnGameBoard.TicTacArrays[i, j] == "O")
                    {
                        Console.Write(ChangesOnGameBoard.TicTacArrays[i, j]);
                    }
                    else
                    {
                        Console.Write(ChangesOnGameBoard.TicTacArrays[i, j] = " ");
                    }
                    Console.Write(" ");
                    Console.Write("|");
                }
                Console.Write(" " + i);
                Console.WriteLine();
                for (int j = 0; j < x; j++)
                {
                    Console.Write(" ---");
                }
                Console.WriteLine();
            }
            letter = 'A';
            Console.Write(" ");
            for (int i = 1; i <= x; i++)
            {
                Console.Write(" " + (letter++) + "  ");
            }
        }
    }
}
